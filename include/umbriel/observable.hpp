/**
 * @file observable.hpp
 * @author dreamyeyed
 *
 * This file implements a simple Observer (aka Listener) pattern.
 */

#pragma once

#include <functional>
#include <vector>

namespace umbriel
{
    /**
     * A class Observable<Event> can fire events of type Event. These events can be
     * observed by observers. An observer can be of any type, as long as it can be
     * called with a single Event parameter.
     */
    template<typename Event>
    class Observable
    {
        public:
        /**
         * Adds a new observer of type Observer. The observer must be able to
         * be called like
         *   observer(const Event& evt)
         */
        template<typename Observer>
        void add_observer(Observer&& observer) {
            observers.push_back(std::forward<Observer>(observer));
        }

        virtual ~Observable()
        {
        }

        protected:
        /**
         * Fires an event that will be sent to all observers.
         */
        void fire_event(const Event &evt) {
            for (auto& observer : observers) {
                observer(evt);
            }
        }

        private:
        std::vector<std::function<void (const Event&)>> observers;
    };
}
