/**
 * @file exception.hpp
 * @author dreamyeyed
 *
 * Standard Umbriel exceptions. Each exception inherits std::exception and also
 * umbriel::Exception.
 */

#pragma once

#include <stdexcept>
#include <string>

namespace umbriel
{
    /**
     * Every exception thrown by Umbriel inherits this class. Its purpose is
     * to distinguish between Umbriel's exceptions and those thrown by other
     * libraries.
     */
    class Exception : public std::exception
    {
        public:
        Exception(const std::string& msg):
            msg(msg)
        {
        }

        const char *what() const noexcept
        {
            return msg.c_str();
        }

        virtual ~Exception()
        {
        }

        private:
        const std::string msg;
    };

    /**
     * A logic exception can be caught at compile time.
     */
    class LogicException : public Exception
    {
        public:
        LogicException(const std::string &msg):
            Exception(msg)
        {
        }

        virtual ~LogicException()
        {
        }
    };

    /**
     * This class represents a runtime exception, i.e. an error that cannot be
     * caught at compile time.
     */
    class RuntimeException : public Exception
    {
        public:
        RuntimeException(const std::string &msg):
            Exception(msg)
        {
        }

        virtual ~RuntimeException()
        {
        }
    };
}
