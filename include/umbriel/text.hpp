/**
 * @file text.hpp
 * @author dreamyeyed
 *
 * Text rendering functions.
 */

#pragma once

#include <string>

#include <SDL2/SDL.h>

namespace umbriel
{
    /**
     * Draws a string using the default font.
     *
     * @param surface surface to draw on
     * @param size tile size
     * @param x x-coordinate (in tiles)
     * @param y y-coordinate (in tiles)
     * @param str text to draw (must be ASCII)
     */
    void draw_string(SDL_Surface *surface, int size, int x, int y, const std::string& str);
}
