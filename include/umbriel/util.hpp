/**
 * @file util.hpp
 * @author dreamyeyed
 *
 * Utility functions and classes that don't belong anywhere else.
 */

#pragma once

#include <SDL2/SDL.h>

namespace umbriel
{
    /**
     * Represents the size of a window, component, etc.
     * Width and height are signed integers to allow "magic" values.
     */
    struct Size
    {
        int width;
        int height;
    };

    /**
     * Represents an area of a window.
     * SDL_Rect contains the size in pixels; this struct adds the size in tiles.
     */
    struct ScreenRect : public SDL_Rect
    {
        public:
        ScreenRect()
        {
        }

        ScreenRect(int x, int y, int w, int h, int tx, int ty, int tw, int th, int ts):
            SDL_Rect{x, y, w, h},
            tile_x(tx), tile_y(ty), tile_w(tw), tile_h(th), tile_size(ts)
        {
        }

        int tile_x;
        int tile_y;
        int tile_w;
        int tile_h;
        int tile_size;
    };
}
