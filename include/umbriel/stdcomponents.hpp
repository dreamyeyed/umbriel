/**
 * @file stdcomponents.hpp
 * @author dreamyeyed
 *
 * This header declares all Components included in Umbriel.
 */

#pragma once

#include <string>

#include "component.hpp"
#include "container.hpp"

namespace umbriel
{
    /**
     * A Box is a simple container where the Components are stacked either
     * vertically or horizontally.
     */
    template<bool horizontal>
    class Box : public Container
    {
        public:
        virtual ~Box();

        void draw(SDL_Surface *surface, const ScreenRect& rect) override;

        private:
        // Calculates tiles available for auto-sized components and how many
        // such components there are
        int leftover_tiles(int total_tiles, int& autosized_components) const;
    };

    // Some useful definitions.
    using VerticalBox = Box<false>;
    using HorizontalBox = Box<true>;

    /**
     * A Label is a simple text component that usually explains something.
     */
    class Label : public Component
    {
        public:
        /**
         * Constructs a label with the given text. The label's width will be
         * equal to the length of this text.
         */
        Label(const std::string& text);

        void draw(SDL_Surface *surface, const ScreenRect& rect) override;

        Size get_size() const override;

        private:
        std::string text;
    };
}
