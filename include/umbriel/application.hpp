/**
 * @file application.hpp
 * @author dreamyeyed
 *
 * Application contains the main loop of Umbriel that handles events, drawing
 * and so on.
 */

#pragma once

#include <SDL2/SDL.h>

#include "component.hpp"
#include "window.hpp"

namespace umbriel
{
    class Application
    {
        public:
        /**
         * Constructor.
         *
         * @param window the window that should be used for this application
         */
        Application(Window window);

        /**
         * Starts the application.
         */
        void run();

        private:
        Window window;
    };
}
