/**
 * @file component.hpp
 * @author dreamyeyed
 *
 * Everything that can be drawn in a window ultimately inherits Component.
 */

#pragma once

#include <memory>

#include <SDL2/SDL.h>

#include "util.hpp"

namespace umbriel
{
    /**
     * A base class for everything that can be drawn in a window. To create a
     * new Component, inherit this and override at least all the abstract
     * member functions.
     */
    class Component : public std::enable_shared_from_this<Component>
    {
        public:
        /**
         * This value means that the size of the Component should be
         * determined automatically.
         */
        static const int AUTO_SIZE = -1;

        Component();

        virtual ~Component();

        // A Component cannot be copied.
        Component(const Component& other) = delete;
        Component& operator=(const Component& other) = delete;

        /**
         * Draws the Component.
         *
         * @param surface the surface to draw on
         * @param rect coordinates and size of the component
         */
        virtual void draw(SDL_Surface *surface, const ScreenRect& rect) = 0;

        /**
         * Returns the size of the Component.
         */
        virtual Size get_size() const;

        /**
         * Returns a pointer to the Component's parent, or nullptr if there
         * isn't one.
         */
        std::weak_ptr<Component> get_parent() const;

        // These two classes can change a component's parent. (This is kind of a
        // hack though...)
        friend class Container;
        friend class Window;

        private:
        std::weak_ptr<Component> parent;
    };
}
