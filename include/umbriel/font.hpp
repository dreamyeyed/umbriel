/**
 * @file font.hpp
 * @author dreamyeyed
 *
 * This file declares a Font class that represents a bitmap font.
 */

#pragma once

#include <SDL2/SDL.h>

namespace umbriel
{
    class Font
    {
        public:
        /**
         * Constructs a font from the given surface. The font is assumed to be
         * ASCII with 16 characters per row (and 8 rows). The background should
         * be magenta (#ff00ff) and the text should be black.
         *
         * This class will own the given surface, so don't use it anywhere else.
         */
        Font(SDL_Surface *surface);

        /**
         * Constructs the default font.
         */
        Font();

        ~Font();

        /**
         * Draws a single letter in this font on the surface.
         *
         * @param surface surface to draw on
         * @param size size in pixels (equal to tile size)
         * @param x x-coordinate (in tiles, *not* in pixels!)
         * @param y y-coordinate (in tiles)
         * @param c letter to draw (must be ASCII)
         */
        void draw_letter(SDL_Surface *surface, int size, int x, int y, char c);

        private:
        SDL_Surface *font;
    };
}
