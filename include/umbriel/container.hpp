/**
 * @file container.hpp
 * @author dreamyeyed
 *
 * This file defines an abstract Container, which is a Component that contains
 * other Components.
 */

#pragma once

#include <memory>
#include <vector>
#include <cstddef>

#include "component.hpp"
#include "util.hpp"

namespace umbriel
{
    class Container : public Component
    {
        public:
        virtual ~Container();

        virtual void draw(SDL_Surface *surface, const ScreenRect& rect) override = 0;

        /**
         * Adds a Component to this Container.
         */
        void add_component(std::shared_ptr<Component> component);

        /**
         * Counts the Components in this Container.
         */
        size_t num_components() const;

        /**
         * Gets the nth Component in this Container.
         */
        std::shared_ptr<Component> get_component(size_t n) const;

        private:
        std::vector<std::shared_ptr<Component>> components;
    };
}
