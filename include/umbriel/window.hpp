/**
 * @file window.hpp
 * @author dreamyeyed
 *
 * Umbriel windows are SDL windows with a few extra operations.
 */

#pragma once

#include <memory>
#include <string>
#include <cstddef>

#include <SDL2/SDL.h>

#include "component.hpp"

namespace umbriel
{
    /**
     * A Window is a wrapper around SDL_Window. Each Window has a root
     * Component, which can then contain other components.
     */
    class Window
    {
        public:
        /**
         * Constructor.
         * 
         * @param title the window's title (shown in titlebar etc.)
         * @param root root component of the window (can be nullptr)
         */
        Window(
            const std::string& title,
            std::shared_ptr<Component> root
        );

        virtual ~Window();

        // A Window cannot be copied...
        Window(const Window& other) = delete;
        Window& operator=(const Window& other) = delete;

        // but it can be moved.
        Window(Window&& other);
        Window& operator=(Window&& other);

        /**
         * Changes the window's root component.
         */
        void set_root(std::shared_ptr<Component> root);

        /**
         * Changes the window's size. If width or height is not divisible by
         * tile_size, it will be rounded.
         */
        void set_size(int width, int height);

        /**
         * Draws the window's contents.
         */
        void draw();

        private:
        SDL_Window *sdl_window;
        std::shared_ptr<Component> root_component;
        const size_t tile_size;
    };
}
