#include "umbriel/container.hpp"

#include <sstream>

#include "umbriel/exception.hpp"

namespace umbriel
{
    Container::~Container()
    {
    }

    void Container::add_component(std::shared_ptr<Component> component)
    {
        if (!component) {
            throw RuntimeException("umbriel::Container::add_component: null pointer given");
        }

        components.push_back(component);
        component->parent = shared_from_this();
    }

    size_t Container::num_components() const
    {
        return components.size();
    }

    std::shared_ptr<Component> Container::get_component(size_t n) const
    {
        if (components.size() <= n) {
            std::ostringstream ss;
            ss << "umbriel::Container::get_component: invalid index " << n;
            throw RuntimeException(ss.str());
        }

        return components[n];
    }
}
