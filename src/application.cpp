#include "umbriel/application.hpp"

namespace umbriel
{
    Application::Application(Window window):
        window(std::move(window))
    {
    }

    void Application::run()
    {
        bool quit = false;
        while (!quit) {
            window.draw();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                switch (event.type) {
                    case SDL_QUIT:
                    quit = true;

                    case SDL_WINDOWEVENT:
                    if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                        window.set_size(event.window.data1, event.window.data2);
                    }
                }
            }
        }
    }
}
