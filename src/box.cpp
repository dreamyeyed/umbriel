#include "umbriel/stdcomponents.hpp"

namespace umbriel
{
    template<bool horizontal>
    Box<horizontal>::~Box()
    {
    }

    template<bool horizontal>
    void Box<horizontal>::draw(SDL_Surface *surface, const ScreenRect& rect)
    {
        // number of tiles available for components
        int total_tiles = horizontal ? rect.tile_w : rect.tile_h;
        // number of components that don't have a specific size
        int autosized_components;
        // number of tiles available for auto-sized components
        int unused_tiles = leftover_tiles(total_tiles, autosized_components);

        int current_tile = horizontal ? rect.tile_x : rect.tile_y;
        const float tiles_per_component = (float)unused_tiles / autosized_components;
        const int tiles_per_component_rounded =
            (autosized_components > 0) ? (unused_tiles / autosized_components)
                                       : 0;
        const float error_per_tile = tiles_per_component - tiles_per_component_rounded;
        float error = 0.0f;
        for (size_t i = 0; i < num_components(); ++i) {
            std::shared_ptr<Component> component = get_component(i);
            Size size = component->get_size();
            ScreenRect component_rect(rect);
            if (horizontal) {
                component_rect.tile_x = current_tile;
                component_rect.x = current_tile * component_rect.tile_size;

                if (size.width == Component::AUTO_SIZE) {
                    component_rect.tile_w = tiles_per_component;
                    error += error_per_tile;
                    if (error >= 0.5f) {
                        component_rect.tile_w += 1;
                        error -= 1.0f;
                    }
                } else {
                    component_rect.tile_w = size.width;
                }

                current_tile += component_rect.tile_w;
                component_rect.w = component_rect.tile_w * component_rect.tile_size;
            } else {
                component_rect.tile_y = current_tile;
                component_rect.y = current_tile * component_rect.tile_size;

                if (size.height == Component::AUTO_SIZE) {
                    component_rect.tile_h = tiles_per_component;
                    error += error_per_tile;
                    if (error >= 0.5f) {
                        component_rect.tile_h += 1;
                        error -= 1.0f;
                    }
                } else {
                    component_rect.tile_h = size.height;
                }

                current_tile += component_rect.tile_h;
                component_rect.h = component_rect.tile_h * component_rect.tile_size;
            }

            // Now that we have finally calculated the size, we can render
            // the component.
            component->draw(surface, component_rect);
        }
    }

    template<bool horizontal>
    int Box<horizontal>::leftover_tiles(int total_tiles, int& autosized_components) const
    {
        autosized_components = 0;
        for (size_t i = 0; i < num_components(); ++i) {
            Size size = get_component(i)->get_size();
            if (horizontal) {
                if (size.width == Component::AUTO_SIZE) {
                    ++autosized_components;
                } else {
                    total_tiles -= size.width;
                }
            } else {
                if (size.height == Component::AUTO_SIZE) {
                    ++autosized_components;
                } else {
                    total_tiles -= size.height;
                }
            }
        }
        return total_tiles;
    }

    // Force compiler to generate the two possible boxes
    template class Box<false>;
    template class Box<true>;
}
