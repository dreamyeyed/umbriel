#include "umbriel/font.hpp"

#include <sstream>
#include <cassert>

#include <SDL2/SDL_image.h>

#include "umbriel/exception.hpp"
#include "umbriel/umbriel_font.hpp"

namespace umbriel
{
    Font::Font(SDL_Surface *surface):
        font(surface)
    {
        if (!font) {
            throw LogicException("umbriel::Font::Font: received null pointer");
        }

        SDL_SetColorKey(font, SDL_TRUE, SDL_MapRGB(surface->format, 0xFF, 0, 0xFF));
    }

    Font::Font()
    {
        // Apparently this function isn't const-correct.
        font = IMG_ReadXPMFromArray(const_cast<char **>(UMBRIEL_FONT_XPM));
        if (!font) {
            std::ostringstream ss;
            ss << "umbriel::Font::Font: default font construction failed: ";
            ss << IMG_GetError();
            throw RuntimeException(ss.str());
        }
    }

    Font::~Font()
    {
        SDL_FreeSurface(font);
    }

    void Font::draw_letter(SDL_Surface *surface, int size, int x, int y, char c)
    {
        assert(font != nullptr);

        if (!surface) {
            throw LogicException("umbriel::Font::draw_letter: surface is null");
        } else if (size <= 0) {
            throw LogicException("umbriel::Font::draw_letter: negative size given");
        }

        // EVIL HACK AHEAD!! It fixes the problems with loading a surface from
        // XPM though
        if (font->format->format != surface->format->format) {
            SDL_Surface *tmp = SDL_ConvertSurface(font, surface->format, 0);
            if (tmp) {
                SDL_FreeSurface(font);
                font = tmp;
                SDL_SetColorKey(font, SDL_TRUE, SDL_MapRGB(font->format, 0xFF, 0, 0xFF));
            }
        }

        SDL_Rect src_rect = {0, 0, font->w/16, font->h/8};
        src_rect.x = (c % 16) * (font->w/16);
        src_rect.y = (c / 16) * (font->h/8);
        SDL_Rect dest_rect = {x * size, y * size, size, size};

        int err = SDL_BlitScaled(font, &src_rect, surface, &dest_rect);
        if (err < 0) {
            std::ostringstream ss;
            ss << "umbriel::Font::draw_letter: SDL_BlitScaled failed with error: ";
            ss << SDL_GetError();
            throw RuntimeException(ss.str());
        }
    }
}
