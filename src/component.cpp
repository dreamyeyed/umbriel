#include "umbriel/component.hpp"

namespace umbriel
{
    Component::Component():
        parent(std::weak_ptr<Component>())
    {
    }

    Component::~Component()
    {
    }

    Size Component::get_size() const
    {
        return {AUTO_SIZE, AUTO_SIZE};
    }

    std::weak_ptr<Component> Component::get_parent() const
    {
        return parent;
    }
}
