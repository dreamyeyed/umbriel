#include "umbriel/stdcomponents.hpp"

#include "umbriel/text.hpp"

namespace umbriel
{
    Label::Label(const std::string& text):
        text(text)
    {
    }

    void Label::draw(SDL_Surface *surface, const ScreenRect& rect)
    {
        draw_string(surface, rect.tile_size, rect.tile_x, rect.tile_y, text);
    }

    Size Label::get_size() const
    {
        // add 1 for padding
        return {static_cast<int>(text.length()) + 1, 1};
    }
}
