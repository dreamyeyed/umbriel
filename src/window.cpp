#include <cstdlib>

#include "umbriel/exception.hpp"
#include "umbriel/window.hpp"

namespace umbriel
{
    Window::Window(const std::string& title, std::shared_ptr<Component> root):
        sdl_window(nullptr),
        root_component(root),
        tile_size(16)
    {
        if (!SDL_WasInit(SDL_INIT_VIDEO)) {
            throw LogicException(
                "You must initialize SDL video subsystem before using Umbriel"
            );
        }

        sdl_window = SDL_CreateWindow(
            title.c_str(),
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            640,
            480,
            SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE
        );
    }

    Window::~Window()
    {
        if (sdl_window) {
            SDL_DestroyWindow(sdl_window);
        }
    }

    Window::Window(Window&& other):
        sdl_window(other.sdl_window),
        root_component(other.root_component),
        tile_size(other.tile_size)
    {
        other.sdl_window = nullptr;
        other.root_component.reset();
    }

    Window& Window::operator=(Window&& other)
    {
        sdl_window = other.sdl_window;
        root_component = other.root_component;
        other.sdl_window = nullptr;
        other.root_component.reset();
        return *this;
    }

    void Window::set_root(std::shared_ptr<Component> root)
    {
        root_component = root;
        root->parent = std::weak_ptr<Component>();
    }

    void Window::set_size(int width, int height)
    {
        if (width % tile_size != 0) {
            int low = (width / tile_size) * tile_size;
            int high = low + tile_size;
            if (std::abs(width - low) < std::abs(width - high)) {
                width = low;
            } else {
                width = high;
            }
        }
        if (height % tile_size != 0) {
            int low = (height / tile_size) * tile_size;
            int high = low + tile_size;
            if (std::abs(height - low) < std::abs(height - high)) {
                height = low;
            } else {
                height = high;
            }
        }
        if (width <= 0 || height <= 0) {
            throw LogicException("umbriel::Window::set_size: non-positive window size given");
        }
        SDL_SetWindowSize(sdl_window, width, height);
    }

    void Window::draw()
    {
        if (!sdl_window) {
            throw RuntimeException("umbriel::Window::draw(): no SDL window?");
        }

        SDL_Surface *surface = SDL_GetWindowSurface(sdl_window);
        if (!surface) {
            throw RuntimeException(SDL_GetError());
        }

        int width, height;
        SDL_GetWindowSize(sdl_window, &width, &height);
        ScreenRect window_rect {0, 0, width, height,
                                0, 0, (int)(width/tile_size), (int)(height/tile_size),
                                (int)tile_size};

        // In the future this could be changed so that only changed components
        // are re-drawn. This works for now, though.
        SDL_FillRect(
            surface,
            &window_rect,
            SDL_MapRGB(surface->format, 0xff, 0xff, 0xff)
        );

        if (root_component) {
            root_component->draw(surface, window_rect);
        }

        SDL_UpdateWindowSurface(sdl_window);
    }
}
