#include "umbriel/text.hpp"

#include "umbriel/font.hpp"

namespace
{
    //default font
    umbriel::Font default_font;
}

namespace umbriel
{
    void draw_string(SDL_Surface *surface, int size, int x, int y, const std::string& str)
    {
        for (const auto& c : str) {
            default_font.draw_letter(surface, size, x, y, c);
            ++x;
        }
    }
}
