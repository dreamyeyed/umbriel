SRC_FILES=$(wildcard src/*.cpp)
OBJ_FILES=$(addprefix obj/,$(notdir $(SRC_FILES:.cpp=.o)))
CXX=g++
FLAGS=-Wall -Wextra -Werror -std=c++11 -Iinclude
LIBS=-lSDL2 -lSDL2_image -lumbriel

debug: FLAGS += -O0 -g -DDEBUG
release: FLAGS += -O2 -DNDEBUG

obj/%.o: src/%.cpp
	mkdir -p obj
	${CXX} ${FLAGS} -c -o $@ $<

lib/libumbriel.a: ${OBJ_FILES}
	mkdir -p lib
	ar rcs $@ $^

debug: lib/libumbriel.a
release: lib/libumbriel.a

SAMPLES_SRC_FILES=$(wildcard samples/src/*.cpp)
SAMPLES_EXE_FILES=$(addprefix samples/build/,$(basename $(notdir $(SAMPLES_SRC_FILES))))

samples/build/%: samples/src/%.cpp lib/libumbriel.a
	mkdir -p samples/build
	${CXX} ${FLAGS} ${LIBS} -O0 -g -DDEBUG -Llib -o $@ $< lib/libumbriel.a

samples: $(SAMPLES_EXE_FILES)

clean:
	rm obj/*.o
	rm lib/*.a
	rm samples/build/*
