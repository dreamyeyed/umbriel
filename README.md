Umbriel - a pixel graphics GUI toolkit
======================================

Umbriel is a GUI toolkit written in C++ using SDL. It's intended to resemble
games on classic game consoles (such as NES).

(C) dreamyeyed 2015.
Umbriel is released under GNU GPL v3.0 or later.
