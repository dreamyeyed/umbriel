/**
 * @file windowtest.cpp
 * @author dreamyeyed
 *
 * This program creates an empty window.
 */

#include <SDL2/SDL.h>

#include "umbriel/application.hpp"
#include "umbriel/window.hpp"

using umbriel::Application;
using umbriel::Window;

int main()
{
    SDL_Init(SDL_INIT_VIDEO);

    Window window("Umbriel window test", nullptr);
    Application app(std::move(window));
    app.run();

    SDL_Quit();
}
