/**
 * @file boxes.cpp
 * @author dreamyeyed
 *
 * This program demonstrates the Box containers.
 */

#include <SDL2/SDL.h>

#include "umbriel/application.hpp"
#include "umbriel/component.hpp"
#include "umbriel/stdcomponents.hpp"
#include "umbriel/util.hpp"
#include "umbriel/window.hpp"

using umbriel::Application;
using umbriel::Component;
using umbriel::Container;
using umbriel::HorizontalBox;
using umbriel::ScreenRect;
using umbriel::Size;
using umbriel::VerticalBox;
using umbriel::Window;

/**
 * A colored rectangle.
 */
class Rectangle : public Component
{
    public:
    Rectangle(int r, int g, int b):
        r(r), g(g), b(b)
    {
    }

    void draw(SDL_Surface *surface, const ScreenRect& rect) override
    {
        SDL_FillRect(surface, &rect, SDL_MapRGB(surface->format, r, g, b));
    }

    private:
    int r;
    int g;
    int b;
};

/**
 * A colored bar.
 */
class Bar : public Component
{
    public:
    Bar(int r, int g, int b):
        r(r), g(g), b(b)
    {
    }

    void draw(SDL_Surface *surface, const ScreenRect& rect) override
    {
        SDL_FillRect(surface, &rect, SDL_MapRGB(surface->format, r, g, b));
    }

    Size get_size() const override
    {
        return {Component::AUTO_SIZE, 2};
    }

    private:
    int r;
    int g;
    int b;
};

int main()
{
    SDL_Init(SDL_INIT_VIDEO);

    std::shared_ptr<Container> vbox(new VerticalBox());
    std::shared_ptr<Container> hbox(new HorizontalBox());
    std::shared_ptr<Component> bar1(new Bar(255, 0, 255));
    std::shared_ptr<Component> bar2(new Bar(255, 0, 255));
    std::shared_ptr<Component> rect1(new Rectangle(255, 0, 0));
    std::shared_ptr<Component> rect2(new Rectangle(0, 255, 0));
    std::shared_ptr<Component> rect3(new Rectangle(0, 0, 255));
    vbox->add_component(bar1);
    vbox->add_component(hbox);
    vbox->add_component(bar2);
    hbox->add_component(rect1);
    hbox->add_component(rect2);
    hbox->add_component(rect3);
    Window window("Box container test", vbox);
    Application app(std::move(window));
    app.run();

    SDL_Quit();
}
