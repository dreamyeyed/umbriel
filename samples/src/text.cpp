/**
 * @file text.cpp
 * @author dreamyeyed
 *
 * An example of using labels.
 */

#include <SDL2/SDL.h>

#include "umbriel/application.hpp"
#include "umbriel/stdcomponents.hpp"
#include "umbriel/window.hpp"

using namespace umbriel;

int main()
{
    SDL_Init(SDL_INIT_VIDEO);

    std::shared_ptr<Component> label1(new Label("first label"));
    std::shared_ptr<Component> label2(new Label("second label"));
    std::shared_ptr<Component> label3(new Label("third label"));
    std::shared_ptr<Container> vbox(new VerticalBox);
    std::shared_ptr<Container> hbox(new HorizontalBox);
    hbox->add_component(label2);
    hbox->add_component(label3);
    vbox->add_component(label1);
    vbox->add_component(hbox);
    Application app(Window("text example", vbox));
    app.run();

    SDL_Quit();
}
