/**
 * @file custom_component.cpp
 * @author dreamyeyed
 *
 * This program creates and tests a custom component that paints some squares.
 * Not very interesting, but it's a start...
 */

#include <SDL2/SDL.h>

#include "umbriel/application.hpp"
#include "umbriel/component.hpp"
#include "umbriel/util.hpp"
#include "umbriel/window.hpp"

using umbriel::Application;
using umbriel::Component;
using umbriel::ScreenRect;
using umbriel::Window;

class Squares : public Component
{
    public:
    void draw(SDL_Surface *surface, const ScreenRect& rect)
    {
        SDL_Rect ul = {rect.x, rect.y, rect.w / 2, rect.h / 2};
        SDL_Rect ur = {rect.x + rect.w/2, rect.y, rect.w / 2, rect.h / 2};
        SDL_Rect ll = {rect.x, rect.y + rect.h/2, rect.w / 2, rect.h / 2};
        SDL_Rect lr = {rect.x+ rect.w/2, rect.y + rect.h/2, rect.w / 2, rect.h / 2};
        SDL_FillRect(surface, &ul, SDL_MapRGB(surface->format, 255, 0, 0));
        SDL_FillRect(surface, &ur, SDL_MapRGB(surface->format, 0, 255, 0));
        SDL_FillRect(surface, &ll, SDL_MapRGB(surface->format, 0, 0, 255));
        SDL_FillRect(surface, &lr, SDL_MapRGB(surface->format, 255, 255, 255));
    }
};

int main()
{
    SDL_Init(SDL_INIT_VIDEO);

    std::shared_ptr<Component> component(new Squares());
    Window window("Custom component test", component);
    Application app(std::move(window));
    app.run();

    SDL_Quit();
}
